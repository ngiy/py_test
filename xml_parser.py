#!/usr/bin/env python
# -*- coding: utf-8 -*-
import xml.etree.cElementTree as ET
import sys
import os
import csv
csv.field_size_limit(sys.maxsize)

header = ["title", "date", "referencenumber", "url", "company", "city", "state", "country", "postalcode", "description", "salary", "education", "jobtype", "category", "experience"]

def xmlToCsv(outfile, xmltree):
    f = open(outfile, "wb")
    writer = csv.writer(f)
    root = xmltree.getroot()
    writer.writerow(header)
    for job in root.iter('job'):
        line = map(lambda x: nilToEmpty(job.find(x)).encode('utf8'), header)
        writer.writerow(line)
    f.close()

def nilToEmpty(s):
    if s is None:
        return ''
    return s.text or ''

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print "第一引数にxmlのファイルパスを指定してください"
        exit(1)

    basedir = os.path.dirname(sys.argv[1])
    filename = os.path.abspath(sys.argv[1])
    xmltree = ET.parse(sys.argv[1])
    xmlToCsv(basedir + filename + ".csv", xmltree)
