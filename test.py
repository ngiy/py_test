from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.etree import ElementTree
from xml.dom import minidom

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    print rough_string
    reparsed = minidom.parseString('<?mso-application progid="Excel.Sheet"?>' + rough_string)
    return reparsed.toprettyxml(indent="  ")

top = Element('Workbook')
top.set('xmlns', 'urn:schemas-microsoft-com:office:spreadsheet')
top.set('xmlns:o', 'urn:schemas-microsoft-com:office:office')
top.set('xmlns:o', 'urn:schemas-microsoft-com:office:office')
top.set('xmlns:x', 'urn:schemas-microsoft-com:office:excel')
top.set('xmlns:ss', 'urn:schemas-microsoft-com:office:spreadsheet')
top.set('xmlns:html', 'http://www.w3.org/TR/REC-html40')

head1 = SubElement(top, 'ExcelWorkbook')
head1.set('xmlns', 'urn:schemas-microsoft-com:office:excel')

headWindowHeight = SubElement(head1, 'WindowHeight')
headWindowHeight.text = '9345'
headWindowWidth = SubElement(head1, 'WindowWidth')
headWindowWidth.text = '16035'
headWindowTopX = SubElement(head1, 'WindowTopX')
headWindowTopX.text = '0'
headWindowTopY = SubElement(head1, 'WindowTopY')
headWindowTopY.text = '30'
headProtectStructure = SubElement(head1, 'ProtectStructure')
headProtectStructure.text = 'False'
headProtectWindows = SubElement(head1, 'ProtectWindows')
headProtectWindows.text = 'False'

comment = Comment('Generated for PyMOTW')
top.append(comment)

child = SubElement(top, 'child')
child.text = 'This child contains text.'

child_with_tail = SubElement(top, 'child_with_tail')
child_with_tail.text = 'This child has regular text.'
child_with_tail.tail = 'And "tail" text.'

child_with_entity_ref = SubElement(top, 'child_with_entity_ref')
child_with_entity_ref.text = 'This & that'

print prettify(top)
