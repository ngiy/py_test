# sep.pyについて
csvのdiffツールです

## 前提
* Mobaxtermの場合は、pythonプラグインをインストール済みであること。
* sep.pyをDesktopに置くこと。  
* diff対象のファイルを両方ともDesktopに置くこと。  
* diff対象のファイルは両方ともUTF-8であること。
* IDは重複がないこと。重複がある場合は、重複のないよう分割する。

## Mobaxtermでの実行方法

### IDが先頭のカラムの場合
* 容量の小さいCSVの場合  
```
python ~/Desktop/sep.py 比較元ファイル 比較先ファイル
```

* 容量の大きいCSVの場合(-s数字、の数字分自動で分割される)  
```
python ~/Desktop/sep.py -s2 比較元ファイル 比較先ファイル
```

* 容量の大きいCSVの場合(-i数字、の数字indexの値ごとに分割される)  
```
python ~/Desktop/sep.py -i2 比較元ファイル 比較先ファイル
```

* 容量の小さいTSVの場合  
```
python ~/Desktop/sep.py -t 比較元ファイル 比較先ファイル
```

* 容量の大きいTSVの場合(-s数字、の数字分自動で分割される)  
```
python ~/Desktop/sep.py -t -s2 比較元ファイル 比較先ファイル
```

* 容量の大きいTSVの場合(-i数字、の数字indexの値ごとに分割される)  
```
python ~/Desktop/sep.py -t -i2 比較元ファイル 比較先ファイル
```

### IDが先頭のカラムではない場合(-k数字、数字部分がIDのindex)
* 容量の小さいCSVの場合  
```
python ~/Desktop/sep.py -k1 比較元ファイル 比較先ファイル
```

* 容量の大きいCSVの場合(-s数字、の数字分自動で分割される)  
```
python ~/Desktop/sep.py -k1 -s2 比較元ファイル 比較先ファイル
```

* 容量の大きいCSVの場合(-i数字、の数字indexの値ごとに分割される)  
```
python ~/Desktop/sep.py -k1 -i2 比較元ファイル 比較先ファイル
```

* 容量の小さいTSVの場合  
```
python ~/Desktop/sep.py -t -k1 比較元ファイル 比較先ファイル
```

* 容量の大きいTSVの場合(-s数字、の数字分自動で分割される)  
```
python ~/Desktop/sep.py -t -k1 -s2 比較元ファイル 比較先ファイル
```

* 容量の大きいTSVの場合(-i数字、の数字indexの値ごとに分割される)  
```
python ~/Desktop/sep.py -t -k1 -i2 比較元ファイル 比較先ファイル
```

## 出力ファイル
* 比較元ファイルの分割ファイル
```
Desktop/実行時間/left/ の中
```

* 比較先ファイルの分割ファイル
```
Desktop/実行時間/right/ の中
```

* 比較結果ファイル
```
Desktop/実行時間/diff_分割名.csv
```
