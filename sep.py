#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import locale
import string
import hashlib
import os
import sys
import csv
from optparse import OptionParser
csv.field_size_limit(sys.maxsize)

exec_date = datetime.datetime.today().strftime("%Y%m%d_%H%M%S")
desktop_dir = os.getenv("HOME") + "/Desktop/"
base_dir = desktop_dir + exec_date
l_dir = base_dir + "/left/"
r_dir = base_dir + "/right/"

header = ""
sepnames = []

def toCode(arg, sep_i):
    return reduce(lambda t, s: t + ord(s), hashlib.sha1(arg).hexdigest(), 0) % sep_i

def separate(args, option):
    if option.sep_index != "":
        sep_common_2(desktop_dir + args[0], option, l_dir)
        sep_common_2(desktop_dir + args[1], option, r_dir)
    else :
        sep_common(desktop_dir + args[0], option, l_dir)
        sep_common(desktop_dir + args[1], option, r_dir)

def sep_common_2(f, option, dirct):
    os.makedirs(dirct)
    key_i = int(option.key)
    sep_i = int(option.sep_index)
    writer_map = {}
    reader = csv.reader(open(f, 'rb'), delimiter=option.delimiter, quotechar=option.quote, lineterminator="\n")
    if not option.header:
        global header
        header = next(reader)
    for l in reader:
        i = l[sep_i]
        if i not in writer_map:
            writer_map[i] = csv.writer(open(dirct + str(i) + ".csv", 'wb'), delimiter=option.delimiter, quotechar=option.quote, lineterminator="\n")
        writer_map[i].writerow(l)
    global sepnames
    sepnames = list(set(sepnames + writer_map.keys()))

def sep_common(f, option, dirct):
    os.makedirs(dirct)
    key_i = int(option.key)
    sep_i = int(option.sep)
    writer_map = {}
    for i in range(sep_i):
        writer = csv.writer(open(dirct + str(i) + ".csv", 'wb'), delimiter=option.delimiter, quotechar=option.quote, lineterminator="\n")
        writer_map[i] = writer
    reader = csv.reader(open(f, 'rb'), delimiter=option.delimiter, quotechar=option.quote, lineterminator="\n")
    if not option.header:
        global header
        header = next(reader)
    for l in reader:
        writer_map[toCode(l[key_i], sep_i)].writerow(l)

def csvdiff(option):
    key_i = int(option.key)
    sep_i = int(option.sep)
    cols = range(sep_i) if len(sepnames) == 0 else sepnames
    for i in cols:
        print " output_diff:" + base_dir + "/diff_" + str(i) + ".csv"
        r_reader = csv.reader(open(r_dir + str(i) + ".csv", 'rb'), delimiter=option.delimiter, quotechar=option.quote, lineterminator="\n")
        r_map = {}
        for l in r_reader:
            r_map[l[key_i]] = l
        w_header = reduce(lambda res, l: res + [l, "L側", "R側"], header, ["key", "相違"])
        l_reader = csv.reader(open(l_dir + str(i) + ".csv", 'rb'), delimiter=option.delimiter, quotechar=option.quote, lineterminator="\n")
        writer = csv.writer(open(base_dir + "/diff_" + str(i) + ".csv", 'wb'), lineterminator="\n")
        writer.writerow(w_header)
        for ll in l_reader:
            rl = r_map.pop(ll[key_i]) if ll[key_i] in r_map else [''] * len(ll)
            target_max_index = max(len(ll), len(rl))
            ll = padlist(ll, target_max_index)
            rl = padlist(rl, target_max_index)
            wl = listdiff(ll, rl, key_i) + reduce(lambda res, x: res + [strdiff(ll[x], rl[x]), ll[x], rl[x]], range(target_max_index), [])
            writer.writerow(wl)
        for rl in r_map.values():
            ll = [''] * len(rl)
            target_max_index = max(len(ll), len(rl))
            wl = listdiff(ll, rl, key_i) + reduce(lambda res, x: res + [strdiff(ll[x], rl[x]), ll[x], rl[x]], range(target_max_index), [])
            writer.writerow(wl)

def padlist(l, sz):
    if (len(l) == sz):
        return l
    return l + ([''] * (sz - len(l)))

def strdiff(left, right):
    diff_bool = left == right
    return "◯" if diff_bool else "×"

def listdiff(ll, rl, key_i):
    target_max_index = max(len(ll), len(rl))
    if ll == [''] * target_max_index:
        return [rl[key_i], "右のみ"]
    if rl == [''] * target_max_index:
        return [ll[key_i], "左のみ"]
    diff_indexes = reduce(lambda res, x: res + ([x] if ll[x] != rl[x] else []), range(target_max_index), [])
    if len(diff_indexes) == 0:
        return [ll[key_i], "相違なし"]
    return [ll[key_i], ",".join(map(lambda x: str(x), diff_indexes))]

def opt_parse(argv):
    opt = OptionParser()
    opt.add_option("-j", action="store_true", dest="header", help="exclude header", default=False)
    opt.add_option("-t", action="store_const", dest="delimiter", const="\t", help="tsv")
    opt.add_option("-n", action="store_const", dest="quote", const="\u0000", help="null quote")
    opt.add_option("-k", action="store", dest="key", default="0", help="join key index")
    opt.add_option("-s", action="store", dest="sep", default="1", help="separate num")
    opt.add_option("-i", action="store", dest="sep_index", default="", help="separate index")
    opt.add_option("-d", action="store", dest="delimiter", help="delimiter", default=",")
    opt.add_option("-u", action="store", dest="quote", help="quote", default="\"")
    opt.add_option("-q", action="store", dest="filter", help="filter ex) \"l[0] == 'id'\"")
    (option, args) = opt.parse_args()
    return option, args

if __name__ == '__main__':
    option, args = opt_parse(sys.argv)
    print 'output_dir:' + os.path.abspath(base_dir)
    print ' left_file:' + args[0]
    print ' right_file:'  + args[1]
    if len(args) != 2:
        sys.stderr.write('arguments is invalid.')
        sys.exit()
    os.makedirs(base_dir)
    separate(args, option)
    csvdiff(option)
    print "normally finish!!!"
