#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import csv
import zipfile
import shutil
from optparse import OptionParser
csv.field_size_limit(sys.maxsize)

# CSVの作成処理
def make_csv(targetcsv):
	baseDir = os.path.dirname(targetcsv)
	targetcsv_name, ext = os.path.splitext(targetcsv)
	outputcsv = targetcsv_name + '_import.csv'
	reader = csv.reader(open(targetcsv, 'rb'), delimiter=',', quotechar='"', lineterminator="\n")
	writer = csv.writer(open(outputcsv, 'wb'), delimiter=',', quotechar='"', lineterminator="\n")
	header = next(reader)
	writer.writerow(header)
	for l in reader:
		itemId = l[0]
		l[3] = "http://www.eheya.net/special/dynamiclp1.html?={itemId}".format(itemId=itemId)
		l[4] = 1
		writer.writerow(l)
	return outputcsv

# zipを解凍
def unzip(baseDir, targetzip):
	if os.path.exists(baseDir): shutil.rmtree(baseDir)
	os.mkdir(baseDir)
	zf = zipfile.ZipFile(targetzip, 'r')
	if len(zf.namelist()) != 1:
		sys.exit("zip file is invalid.")

	f = zf.namelist()[0]
	if not os.path.basename(f):
		os.mkdir(baseDir + '/' + f)
	else:
		uzf = file(baseDir + '/' + f, 'wb')
		uzf.write(zf.read(f))
		uzf.close()

	zf.close()
	return baseDir + '/' + f

def zip(outputzip, outputcsv):
	zipFile = zipfile.ZipFile(outputzip,"w",zipfile.ZIP_DEFLATED)
	zipFile.write(outputcsv, os.path.basename(outputcsv))
	zipFile.close()
	print outputzip

if __name__ == '__main__':
	if len(sys.argv) != 2:
		sys.exit("argument error.")

	baseDir = os.path.dirname(sys.argv[1]) + '/lpotmp'
	targetzip_name, ext = os.path.splitext(sys.argv[1])
	outputcsv = make_csv(unzip(baseDir, sys.argv[1]))
	outputzip = targetzip_name + "_import.zip"
	zip(outputzip, outputcsv)

	shutil.rmtree(baseDir)
