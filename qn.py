#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import csv
from optparse import OptionParser
csv.field_size_limit(sys.maxsize)

def full(cond, cols, option):
	reader = csv.reader(sys.stdin, delimiter=option.delimiter, quotechar=option.quote, lineterminator="\n")
	writer = csv.writer(sys.stdout, lineterminator="\n")
	if option.header:
		header = next(reader)
	for l in reader:
		if not cond or eval(cond):
			disp(writer, l, cols)

def disp(writer, l, cols):
	cols = cols or range(len(l))
	writer.writerow(map(lambda n: l[n], cols))

def desc(cond, cols, option):
	reader = csv.reader(sys.stdin, delimiter=option.delimiter, quotechar=option.quote, lineterminator="\n")
	writer = csv.writer(sys.stdout, lineterminator="\n")
	if option.header:
		header = next(reader)
	for l in reader:
		if not cond or eval(cond):
			desc_disp(writer, l, cols)
			return

def desc_disp(writer, l, cols):
	cols = cols or range(len(l))
	for n in cols:
		print "[{n}] : {v}".format(n=n, v=l[n])

def opt_parse(argv):
	opt = OptionParser()
	opt.add_option("--desc", action="store_true", help="describe a line [index] : value")
	opt.add_option("-j", action="store_true", dest="header", help="exclude header", default=False)
	opt.add_option("-t", action="store_const", dest="delimiter", const="\t", help="tsv")
	opt.add_option("-s", action="store_const", dest="delimiter", const="\x12", help="dc2")
	opt.add_option("-n", action="store_const", dest="quote", const="\u0000", help="null quote")
	opt.add_option("-d", action="store", dest="delimiter", help="delimiter", default=",")
	opt.add_option("-u", action="store", dest="quote", help="quote", default="\"")
	opt.add_option("-q", action="store", dest="filter", help="filter ex) \"l[0] == 'id'\"")
	opt.add_option("-C", action="store", dest="cols", help="output column indexes ex) 0,1,3")
	(option, args) = opt.parse_args()
	return option

if __name__ == '__main__':
	option = opt_parse(sys.argv)
	cols = []
	if option.cols and len(option.cols.split(",")) != 0:
		cols = map(lambda n: int(n), option.cols.split(","))
	if option.desc:
		desc(option.filter, cols, option)
	else:
		full(option.filter, cols, option)
